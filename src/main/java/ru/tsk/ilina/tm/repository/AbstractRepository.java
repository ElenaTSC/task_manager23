package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.model.AbstractEntity;
import ru.tsk.ilina.tm.model.User;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IAbstractRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E add(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(@NotNull final String id) {
        for (final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeById(@NotNull final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        return remove(entity);
    }

    @Override
    public E remove(@NotNull final E entity) {
        entities.remove(entity);
        return entity;
    }

}
