package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @Nullable final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId())).collect(Collectors.toList());
    }

    @Override
    public Task unbindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @Nullable final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(o -> entities.remove(o.getId()));
    }

}
