package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IAbstractOwnerRepository<E> {

    public static Predicate<AbstractOwnerEntity> predicateById(@NotNull final String userId) {
        return o -> userId.equals(o.getUserId());
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).remove(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getId()));
    }

    @Override
    public Integer getSize(@NotNull final String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).size();
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.stream().filter(predicateById(userId)).sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final List<E> entity = findAll(userId);
        return entity.get(index);
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return Optional.ofNullable(findByID(userId, id)).isPresent();
    }

    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

    @Override
    public E findByID(@NotNull final String userId, @NotNull final String id) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && id.equals(o.getId())).findFirst().orElse(null);
    }

    @Override
    public E removeByID(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findByID(userId, id));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

}
