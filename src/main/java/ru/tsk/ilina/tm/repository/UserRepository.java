package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IUserRepository;
import ru.tsk.ilina.tm.model.User;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(@NotNull final String email) {
        return entities.stream().filter(o -> email.equals(o.getEmail())).findFirst().orElse(null);
    }

    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.stream().filter(o -> login.equals(o.getLogin())).findFirst().orElse(null);
    }

}
