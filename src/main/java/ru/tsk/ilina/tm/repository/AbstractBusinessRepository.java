package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.*;

import java.util.Optional;

@NoArgsConstructor
public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractOwnerRepository<E> implements IAbstractBusinessRepository<E> {

    public Optional<E> updateStatus(@NotNull final E entity, @NotNull final Status status) {
        Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> e.setStatus(status));
        return optional;
    }

    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        final E entity = findByName(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName())).findFirst().orElse(null);
    }

    @Override
    public E startByID(@NotNull final String userId, @NotNull final String id) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E startByName(@NotNull final String userId, @NotNull final String name) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, Status.IN_PROGRESS).orElse(null);
    }

    @Override
    public E changeStatusByID(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E finishByID(@NotNull final String userId, @NotNull final String id) {
        final E entity = findByID(userId, id);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

    @Override
    public E finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final E entity = findByIndex(userId, index);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

    @Override
    public E finishByName(@NotNull final String userId, @NotNull final String name) {
        final E entity = findByName(userId, name);
        return updateStatus(entity, Status.COMPLETED).orElse(null);
    }

}
