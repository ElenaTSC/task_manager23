package ru.tsk.ilina.tm.enumerated;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

}
