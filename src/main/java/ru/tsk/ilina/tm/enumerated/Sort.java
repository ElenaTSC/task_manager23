package ru.tsk.ilina.tm.enumerated;

import lombok.Getter;
import ru.tsk.ilina.tm.comparator.ComparatorByCreated;
import ru.tsk.ilina.tm.comparator.ComparatorByName;
import ru.tsk.ilina.tm.comparator.ComparatorByStartDate;
import ru.tsk.ilina.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

@Getter
public enum Sort {

    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    CREATE_DATE("Sort by create date", ComparatorByCreated.getInstance());

    private final String displayName;
    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
