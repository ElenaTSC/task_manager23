package ru.tsk.ilina.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.service.IAbstractBusinessService;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.*;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;
import ru.tsk.ilina.tm.model.Task;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity, R extends IAbstractBusinessRepository<E>> extends AbstractOwnerService<E, R> implements IAbstractBusinessService<E> {

    protected AbstractBusinessService(R repository) {
        super(repository);
    }

    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    public E updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (id.isEmpty()) throw new EmptyIdException();
        final E entity = repository.findByID(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E changeStatusByID(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByID(userId, id, status);
    }

    @Override
    public E changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByName(userId, name, status);
    }

    @Override
    public E finishByName(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        return repository.finishByName(userId, name);
    }

    public E startByName(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        return repository.startByName(userId, name);
    }

    public E finishByID(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.finishByID(userId, id);
    }

    public E startByID(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.startByID(userId, id);
    }

    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        return repository.removeByIndex(userId, index);
    }

    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        return repository.findByIndex(userId, index);
    }

    @Override
    public E updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @Nullable final String description) {
        if (name.isEmpty()) throw new EmptyNameException();
        if (index < 0) throw new EmptyIdException();
        if (repository.getSize(userId) < index - 1) throw new ProjectNotFoundException();
        final E entity = repository.findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final E entity = findByIndex(userId, index);
        repository.startByIndex(userId, index);
        return entity;
    }

    @Override
    public E finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final E entity = findByIndex(userId, index);
        repository.finishByIndex(userId, index);
        return entity;
    }

    @Override
    public E changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        return repository.changeStatusByIndex(userId, index, status);
    }

}
