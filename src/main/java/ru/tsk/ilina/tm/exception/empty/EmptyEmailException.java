package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty");
    }

    public EmptyEmailException(String message) {
        super("Error! " + message + " email is empty");
    }

}
