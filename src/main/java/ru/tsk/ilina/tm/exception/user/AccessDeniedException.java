package ru.tsk.ilina.tm.exception.user;

import ru.tsk.ilina.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied");
    }

    public AccessDeniedException(String message) {
        super("Error! " + message + " access denied");
    }

}
