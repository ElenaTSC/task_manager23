package ru.tsk.ilina.tm.command;

import ru.tsk.ilina.tm.api.service.IServiceLocator;
import ru.tsk.ilina.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String description();

    public abstract String arg();

    public abstract void execute();

    public Role[] roles() {
        return null;
    }

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String description = description();
        String arg = arg();
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "]";
        if (description != null && !description.isEmpty()) result += "[" + description + "]";
        return result;
    }

}
