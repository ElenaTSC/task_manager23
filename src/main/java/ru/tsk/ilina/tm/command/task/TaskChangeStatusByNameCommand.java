package ru.tsk.ilina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task_change_status_by_name";
    }

    @Override
    public String description() {
        return "Change status by task name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final Task task = serviceLocator.getTaskService().changeStatusByName(userId, name, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
