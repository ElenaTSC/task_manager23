package ru.tsk.ilina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthRegistryCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registry in system new user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println(" OK]");
    }

}
