package ru.tsk.ilina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.model.User;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthViewProfileCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "view";
    }

    @Override
    public String description() {
        return "View information about user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

}
