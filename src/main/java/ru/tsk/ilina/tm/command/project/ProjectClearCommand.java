package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Role;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Delete all projects";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
