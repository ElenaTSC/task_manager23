package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.util.NumberUtil;

public final class InfoCommand extends AbstractCommand {

    @Override
    public String name() {
        return "info";
    }

    @Override
    public String description() {
        return "Display system info.";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(runtime.freeMemory()));
        long maxMemory = runtime.maxMemory();
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(runtime.totalMemory()));
        @NotNull final long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Use memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
